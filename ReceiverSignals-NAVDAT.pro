QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = NavdatSignals
TEMPLATE = app

exists(../ReceiverSignalsVars.pri)
{
  include(../ReceiverSignalsVars.pri)
  INCLUDEPATH += $$RECEIVER_SIGNALS_INPUT_DRIVER_PWD
  LIBS += -L$$RECEIVER_SIGNALS_DESTDIR
  DESTDIR = $$RECEIVER_SIGNALS_DESTDIR
}

SOURCES += main.cpp\
        MainWindow.cpp \
    QLed/qledindicator.cpp

HEADERS  += MainWindow.h \
    QLed/qledindicator.h

FORMS    += MainWindow.ui

LIBS += -lNavdatSignalsInputDriver

include(SDR/sdr.pri)

unix {
    target.path = /usr/local/bin
    INSTALLS += target
}

DEFINES += STEP_MODE
INCLUDEPATH += $$PWD/QLed
