#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDataStream>
#include <QVector>

#include <QFileDialog>

#define Ts        ((1.0+1.0/4)*256/12000)
#define AveragingCount 40

TSoprocessor::TSoprocessor(QThread* thread, QObject* parent) :
  QObject(parent)
{
  if (thread)
    moveToThread(thread);

  ScopePlot = 0;
  SpectrumPlot = 0;
  SyncFreqEstimationPlot = 0;
  SyncTettaPlot = 0;
  EqualizerPlot = 0;
  PhasePlot = 0;
  ScatterPlot = 0;
}

TSoprocessor::~TSoprocessor()
{

}

void TSoprocessor::setConfigData(quint32 Fd, quint32 Nfft)
{
  this->Fd = Fd;
  this->Nfft = Nfft;

  SpectrumPlot->setFd(Fd);
  SpectrumPlot->setNfft(Nfft);
}

void TSoprocessor::setScopeData(QVector<float> buf)
{
  if (ScopePlot->IsDataRefresh()) return;
  ScopePlot->append(1.0/Fd, (iqSample_t*)buf.data(), buf.size()/2);
}

void TSoprocessor::setSyncEstimationsData(QVector<float> corr, QVector<float> freq)
{
  if (SyncTettaPlot->IsDataRefresh()) return;
  if (SyncFreqEstimationPlot->IsDataRefresh()) return;

  SyncTettaPlot->append(corr.data(), corr.size());
  SyncFreqEstimationPlot->append(freq.data(), freq.size());
}

void TSoprocessor::setSpectrumData(QVector<float> buf)
{
  if (SpectrumPlot->IsDataRefresh()) return;
  SpectrumPlot->append_fft((iqSample_t*)buf.data(), buf.size()/2);
}

void TSoprocessor::setEqualizerData(QVector<float> buf)
{
  if (EqualizerPlot->IsDataRefresh()) return;
  EqualizerPlot->append(buf.data(), buf.size());
}

void TSoprocessor::setPhaseOffsetData(QVector<float> buf)
{
  if (PhasePlot->IsDataRefresh()) return;
  PhasePlot->append(buf.data(), buf.size());
}

void TSoprocessor::setConstellationData(QVector<float> buf)
{
  if (ScatterPlot->IsDataRefresh()) return;
  ScatterPlot->append((iqSample_t*)buf.data(), buf.size()/2);
}

//---------------------------------------------

void MainWindow::message(QString header, QString msg, int timeout)
{
  ui->statusBar->showMessage(QString("%1: %2").arg(header).arg(msg), timeout);
}

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ui_setConnected(false);

  IsWaitConnection = false;

  measurementFileName = "";
  ui->widgetMER->setVisible(ui->action_outputMER->isChecked());
  ui->widgetBER0->setVisible(ui->action_outputBER0->isChecked());
  ui->widgetBER->setVisible(ui->action_outputBER->isChecked());

  ui->action_outputMER->setVisible(false);
  ui->action_outputBER0->setVisible(false);

  ui->RefreshTime->setValue(60*Ts);
  ui->RefreshTime->setSingleStep(Ts);
  refreshTimer.setInterval(ui->RefreshTime->value()*1000);

  init_sdrQtAddons();
  Driver.addInterface(&navdatIf);
  Soprocessor.moveToThread(Driver.thread());

  init_plots();

  connect(Driver.device(), SIGNAL(enabled()), &navdatIf, SLOT(enable()));
  connect(this, SIGNAL(disableNavdatIf()), &navdatIf, SLOT(disable()));
  connect(this, SIGNAL(disableDevice()), Driver.device(), SLOT(disable()));

  connect(this, SIGNAL(setSignalParams(quint8,ReceiverSignalsInterface::CommonSignalParams,QByteArray)), &navdatIf, SLOT(setSignalParams(quint8,ReceiverSignalsInterface::CommonSignalParams,QByteArray)));
  connect(this, SIGNAL(setSignalEnabled(quint8,bool)), &navdatIf, SLOT(setSignalEnabled(quint8,bool)));

  Fd = 0; Nfft = 0;
  connect(&navdatIf, SIGNAL(ConfigRequestData(quint32,quint32)), this, SLOT(setConfigData(quint32,quint32)));
  connect(&navdatIf, SIGNAL(ConfigRequestData(quint32,quint32)), &Soprocessor, SLOT(setConfigData(quint32,quint32)));
}

MainWindow::~MainWindow()
{
  closeConnection();

  delete ui;

  delete ScopePlot;
  delete SyncFreqEstimationPlot;
  delete SyncCorrelationPlot;
  delete SpectrumPlot;
  delete EqualizerPlot;
  delete PhasePlot;
  delete ScatterPlot;
}

void MainWindow::init_plots()
{
  ScopePlot = new SDR::TScopePlot(ui->ScopePlot);
  ScopePlot->setBaseColors(QColor(20,20,20), Qt::white);
  ui->ScopeTime->setValue(30*Ts);
  ui->ScopeTime->setSingleStep(Ts);
  ScopePlot->setAutoRefresh(false);
  ScopePlot->setAutoRangeY(true);
  ScopePlot->enableZoom(Qt::Horizontal);
  ScopePlot->enableDrag(Qt::Horizontal);
  Soprocessor.setScopePlot(ScopePlot);
#ifndef STEP_MODE
  connect(ScopePlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_scope()));
//  connect(ScopePlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_scope()));
#endif
  connect(ScopePlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), ScopePlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  SyncFreqEstimationPlot = new SDR::TBufferPlot(ui->SyncFreqEstimationPlot);
  SyncFreqEstimationPlot->setBaseColors(QColor(20,20,20), Qt::white);
  SyncFreqEstimationPlot->setAutoRefresh(false);
  SyncFreqEstimationPlot->setAutoRangeY(true);
#ifndef STEP_MODE
  SyncFreqEstimationPlot->setAveragingEnable(true);
  SyncFreqEstimationPlot->setAveragingCount(AveragingCount);
#endif
  Soprocessor.setSyncFreqEstimationPlot(SyncFreqEstimationPlot);
#ifndef STEP_MODE
  connect(SyncFreqEstimationPlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_sync_window()));
//  connect(SyncFreqEstimationPlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_sync_window()));
#endif
  connect(SyncFreqEstimationPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), SyncFreqEstimationPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  SyncCorrelationPlot = new SDR::TBufferPlot(ui->SyncCorrelationPlot);
  SyncCorrelationPlot->setBaseColors(QColor(20,20,20), Qt::white);
  SyncCorrelationPlot->setAutoRefresh(false);
  SyncCorrelationPlot->setAutoRangeY(true);
#ifndef STEP_MODE
  SyncCorrelationPlot->setAveragingEnable(true);
  SyncCorrelationPlot->setAveragingCount(AveragingCount);
#endif
  Soprocessor.setSyncTettaPlot(SyncCorrelationPlot);
#ifndef STEP_MODE
  connect(SyncCorrelationPlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_sync_correlation()));
//  connect(SyncTettaPlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_sync_correlation()));
#endif
  connect(SyncCorrelationPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), SyncCorrelationPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  SpectrumPlot = new SDR::TSpectrumPlot(0, 0, ui->SpectrumPlot);
  SpectrumPlot->setBaseColors(QColor(20,20,20), Qt::white);
  SpectrumPlot->getCustomPlot()->setStyleSheet("color:blue");
  SpectrumPlot->setAutoRefresh(false);
  SpectrumPlot->setAutoRangeY(true);
  SpectrumPlot->enableZoom(Qt::Horizontal);
  SpectrumPlot->enableDrag(Qt::Horizontal);
#ifndef STEP_MODE
  SpectrumPlot->setAveragingEnable(true);
  SpectrumPlot->setAveragingCount(AveragingCount);
#endif
  SpectrumPlot->moveToThread(Soprocessor.thread());
  Soprocessor.setSpectrumPlot(SpectrumPlot);
#ifndef STEP_MODE
  connect(SpectrumPlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_spectrum()));
//  connect(SpectrumPlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_spectrum()));
#endif
  connect(SpectrumPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), SpectrumPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  EqualizerPlot = new SDR::TBufferPlot(ui->EqualizerPlot);
  EqualizerPlot->setBaseColors(QColor(20,20,20), Qt::white);
  EqualizerPlot->setAutoRefresh(false);
  EqualizerPlot->setAutoRangeY(true);
  EqualizerPlot->getGraph()->setLineStyle(QCPGraph::lsImpulse);
#ifndef STEP_MODE
  EqualizerPlot->setAveragingEnable(true);
  EqualizerPlot->setAveragingCount(AveragingCount);
#endif
  Soprocessor.setEqualizerPlot(EqualizerPlot);
#ifndef STEP_MODE
  connect(EqualizerPlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_equalizer()));
//  connect(EqualizerPlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_equalizer()));
#endif
  connect(EqualizerPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), EqualizerPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  PhasePlot = new SDR::TBufferPlot(ui->PhasePlot);
  PhasePlot->setBaseColors(QColor(20,20,20), Qt::white);
  PhasePlot->setAutoRefresh(false);
  PhasePlot->setRangeY(-SDR_PI, SDR_PI);
  PhasePlot->getGraph()->setLineStyle(QCPGraph::lsImpulse);
#ifndef STEP_MODE
  PhasePlot->setAveragingEnable(true);
  PhasePlot->setAveragingCount(AveragingCount);
#endif
  Soprocessor.setPhasePlot(PhasePlot);
#ifndef STEP_MODE
  connect(PhasePlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_phase_offset()));
//  connect(PhasePlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_phase_offset()));
#endif
  connect(PhasePlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), PhasePlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));

  ScatterPlot = new SDR::TScatterPlot(ui->ScatterPlot);
  ScatterPlot->setBaseColors(QColor(20,20,20), Qt::white);
  ScatterPlot->setAutoRefresh(false);
  ScatterPlot->setRangeX(-9, 9);
  ScatterPlot->setRangeY(-9, 9);
  Soprocessor.setScatterPlot(ScatterPlot);
#ifndef STEP_MODE
  connect(ScatterPlot, SIGNAL(refresh_started()), this, SLOT(navdat_disable_constellation()));
//  connect(ScatterPlot, SIGNAL(refreshed()), this, SLOT(navdat_enable_constellation()));
#endif
  connect(ScatterPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), ScatterPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));
}

#define BUTTON_CONNECTION_CONTROL_TEXT(isConnected) ((isConnected)?("Отключиться"):("Подключиться"))
void MainWindow::ui_setConnected(bool flag)
{
  ui->widget_ipSettings->setEnabled(!flag);
  ui->button_connectionControl->setText(BUTTON_CONNECTION_CONTROL_TEXT(flag));
  if (flag)
    setRefreshStateWaiting();
  else
    setRefreshStateDisable();
}

void MainWindow::ui_clearWidgets()
{
  ui->statusBar->clearMessage();

  ui->StartIdx->setText("-");
  ui->SNR->setText("-");
  ui->FreqOffset->setText("-");

  clearPlots();
}

void MainWindow::refreshRequest()
{
  if (stateRefresh == rs_Waiting)
    message("Обновление", "данные от устройства не получены", 0.8*refreshTimer.interval());
  else
  {
    setRefreshStateWaiting();
    clearPlotsData();
  }
  enableSignals(true);
}

void MainWindow::setRefreshStateDisable()
{
  stateRefresh = rs_Disable;
  ui->RefreshStateLed->setOffColor1(Qt::gray);
  ui->RefreshStateLed->setOffColor2(Qt::darkGray);
  ui->RefreshStateLed->setChecked(false);
}

void MainWindow::setRefreshStateWaiting()
{
  stateRefresh = rs_Waiting;
  ui->RefreshStateLed->setOffColor1(Qt::darkGreen);
  ui->RefreshStateLed->setChecked(false);
}

void MainWindow::setRefreshStateUpdated()
{
  stateRefresh = rs_Updated;
  ui->RefreshStateLed->setChecked(true);
}

void MainWindow::clearPlots()
{
  ScopePlot->clear();
  SpectrumPlot->clear();
  SyncFreqEstimationPlot->clear();
  SyncCorrelationPlot->clear();
  EqualizerPlot->clear();
  PhasePlot->clear();
  ScatterPlot->clear();
}

void MainWindow::clearPlotsData()
{
  ScopePlot->clearData();
  SpectrumPlot->clearData();
  SyncFreqEstimationPlot->clearData();
  SyncCorrelationPlot->clearData();
  EqualizerPlot->clearData();
  PhasePlot->clearData();
  ScatterPlot->clearData();
}


void MainWindow::openConnection()
{
  ui_clearWidgets();
  IsWaitConnection = true;
  ui->button_connectionControl->setText("Ожидание");
  Driver.enable(ui->ipAddr_Host->currentText());
}

void MainWindow::closeConnection()
{
  if (!IsWaitConnection)
  {
    emit disableNavdatIf();
    navdatIf_disabled_handler();
  }
  emit disableDevice();

  IsWaitConnection = false;
  ui_setConnected(false);
}

void MainWindow::navdatIf_enabled_handler()
{
  if (!IsWaitConnection) return;

  IsWaitConnection = false;

  setSignalsParams();
  setMeasurmentsParams();

  enableSignals();
  enableMeasurements();

  refreshTimer.start();
  connect(&refreshTimer, SIGNAL(timeout()), this, SLOT(refreshRequest()));

  ui_setConnected(true);

  connect(&navdatIf, SIGNAL(data_readed()), this, SLOT(setRefreshStateUpdated()));
  connect(&navdatIf, SIGNAL(ScopeData(QVector<float>)), &Soprocessor, SLOT(setScopeData(QVector<float>)));
  connect(&navdatIf, SIGNAL(SyncEstimationsData(QVector<float>,QVector<float>)), &Soprocessor, SLOT(setSyncEstimationsData(QVector<float>,QVector<float>)));
  connect(&navdatIf, SIGNAL(SpectrumData(QVector<float>)), &Soprocessor, SLOT(setSpectrumData(QVector<float>)));
  connect(&navdatIf, SIGNAL(SignalDetectedData(quint32)), this, SLOT(setSignalDetectedData(quint32)));
  connect(&navdatIf, SIGNAL(EstimationsData(float,float)), this, SLOT(setEstimationsData(float,float)));
  connect(&navdatIf, SIGNAL(EqualizerData(QVector<float>)), &Soprocessor, SLOT(setEqualizerData(QVector<float>)));
  connect(&navdatIf, SIGNAL(PhaseOffsetData(QVector<float>)), &Soprocessor, SLOT(setPhaseOffsetData(QVector<float>)));
  connect(&navdatIf, SIGNAL(ConstellationData(QVector<float>)), &Soprocessor, SLOT(setConstellationData(QVector<float>)));
  connect(&navdatIf, SIGNAL(qamData(quint8,QVector<qint8>)), this, SLOT(setQamData(quint8,QVector<qint8>)));
  connect(&navdatIf, SIGNAL(encData(quint8,QVector<quint8>)), this, SLOT(setEncData(quint8,QVector<quint8>)));
  connect(&navdatIf, SIGNAL(Data(quint8,QVector<quint8>)), this, SLOT(setData(quint8,QVector<quint8>)));
}

void MainWindow::navdatIf_disabled_handler()
{
  if (IsWaitConnection) return;

  refreshTimer.stop();
  disconnect(&refreshTimer, SIGNAL(timeout()), this, SLOT(refreshRequest()));

  disconnect(&navdatIf, SIGNAL(ScopeData(QVector<float>)), &Soprocessor, SLOT(setScopeData(QVector<float>)));
  disconnect(&navdatIf, SIGNAL(SyncEstimationsData(QVector<float>,QVector<float>)), &Soprocessor, SLOT(setSyncEstimationsData(QVector<float>,QVector<float>)));
  disconnect(&navdatIf, SIGNAL(SpectrumData(QVector<float>)), &Soprocessor, SLOT(setSpectrumData(QVector<float>)));
  disconnect(&navdatIf, SIGNAL(SignalDetectedData(quint32)), this, SLOT(setSignalDetectedData(quint32)));
  disconnect(&navdatIf, SIGNAL(EstimationsData(float,float)), this, SLOT(setEstimationsData(qint16)));
  disconnect(&navdatIf, SIGNAL(EqualizerData(QVector<float>)), &Soprocessor, SLOT(setEqualizerData(QVector<float>)));
  disconnect(&navdatIf, SIGNAL(PhaseOffsetData(QVector<float>)), &Soprocessor, SLOT(setPhaseOffsetData(QVector<float>)));
  disconnect(&navdatIf, SIGNAL(ConstellationData(QVector<float>)), &Soprocessor, SLOT(setConstellationData(QVector<float>)));
  disconnect(&navdatIf, SIGNAL(qamData(quint8,QVector<qint8>)), this, SLOT(setQamData(quint8,QVector<qint8>)));
  disconnect(&navdatIf, SIGNAL(encData(quint8,QVector<quint8>)), this, SLOT(setEncData(quint8,QVector<quint8>)));
  disconnect(&navdatIf, SIGNAL(Data(quint8,QVector<quint8>)), this, SLOT(setData(quint8,QVector<quint8>)));

  emit disableDevice();
}

void MainWindow::setConfigData(quint32 Fd, quint32 Nfft)
{
  this->Fd = Fd;
  this->Nfft = Nfft;

  navdatIf_enabled_handler();
}

void MainWindow::setSignalParams(quint8 signal, QByteArray spec_data)
{
  ReceiverSignalsInterface::CommonSignalParams params;
  switch(signal)
  {
  default:
#ifndef STEP_MODE
    params.mode = ReceiverSignalsInterface::modeStream;
#else
    params.mode = ReceiverSignalsInterface::modeStep;
#endif
    break;

  case ReceiverSignalsNavdatInterface::SignalDetected:
  case ReceiverSignalsNavdatInterface::Estimations:
    params.mode = ReceiverSignalsInterface::modeStream;
    break;
  }
  emit setSignalParams(signal, params, spec_data);
}

void MainWindow::setMeasurementParams(quint8 symbol)
{
  ReceiverSignalsInterface::CommonSignalParams params;
  params.mode = ReceiverSignalsInterface::modeStream;
  emit setSignalParams(symbol, params, QByteArray());
}

void MainWindow::setScopeParams()
{
  setSignalParams(ReceiverSignalsNavdatInterface::Scope, ReceiverSignalsNavdatInterface::convertScopeSpecParamsData(getScopeSamplesCount()));
}

void MainWindow::setSignalsParams()
{
  for (Size_t x = ReceiverSignalsNavdatInterface::Scope; x < ReceiverSignalsNavdatInterface::signalsCount; ++x)
  {
    QByteArray spec;
    switch (x)
    {
    case ReceiverSignalsNavdatInterface::Scope:
      spec = ReceiverSignalsNavdatInterface::convertScopeSpecParamsData(getScopeSamplesCount());
      break;
    default:
      spec.clear();
      break;
    }
    setSignalParams(x, spec);
  }
}

void MainWindow::setMeasurmentsParams()
{
  for (Size_t x = ReceiverSignalsNavdatInterface::qamBuf; x < ReceiverSignalsNavdatInterface::symbolsCount; ++x)
    setMeasurementParams(x);
}

void MainWindow::navdat_enable_scope()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Scope, true);
}

void MainWindow::navdat_disable_scope()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Scope, false);
}

void MainWindow::navdat_enable_spectrum()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Spectrum, true);
}

void MainWindow::navdat_disable_spectrum()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Spectrum, false);
}

void MainWindow::navdat_enable_equalizer()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Equalizer, true);
}

void MainWindow::navdat_disable_equalizer()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Equalizer, false);
}

void MainWindow::navdat_enable_phase_offset()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::PhaseOffset, true);
}

void MainWindow::navdat_disable_phase_offset()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::PhaseOffset, false);
}

void MainWindow::navdat_enable_constellation()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Constellation, true);
}

void MainWindow::navdat_disable_constellation()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Constellation, false);
}

void MainWindow::navdat_enable_sync_estimations()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::SyncEstimations, true);
}

void MainWindow::navdat_disable_sync_estimations()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::SyncEstimations, false);
}

void MainWindow::enableSignals(bool isRequest)
{
  int I = ui->tabSignals->currentIndex();
  bool isEnabled;
  for (int i = 0; i < ReceiverSignalsNavdatInterface::signalsCount; ++i)
  {
    isEnabled = false;
    switch(i)
    {
    case ReceiverSignalsNavdatInterface::Scope:
      if (I != 2) break;
      isEnabled = ui->action_outputScope->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::SyncEstimations:
      if (I != 1) break;
      isEnabled = ui->action_outputSyncEstimations->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::Spectrum:
      if (I != 0) break;
      isEnabled = ui->action_outputSpectrum->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::SignalDetected:
      isEnabled = ui->action_outputSyncStartIdx->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::Estimations:
      isEnabled = ui->action_outputEstimations->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::Equalizer:
      if (I != 0) break;
      isEnabled = ui->action_outputEqualizer->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::PhaseOffset:
      if (I != 0) break;
      isEnabled = ui->action_outputPhaseOffset->isChecked();
      break;
    case ReceiverSignalsNavdatInterface::Constellation:
      if (I != 0) break;
      isEnabled = ui->action_outputConstellation->isChecked();
      break;
    }
    if (!isRequest || (isRequest && isEnabled))
      emit setSignalEnabled(i, isEnabled);
  }
}

void MainWindow::setSignalsEnabled(bool enabled)
{
  ui->action_outputScope->setChecked(enabled);
  ui->action_outputSyncFreqEstimation->setChecked(enabled);
  ui->action_outputSyncEstimations->setChecked(enabled);
  ui->action_outputSpectrum->setChecked(enabled);
  ui->action_outputSyncStartIdx->setChecked(enabled);
  ui->action_outputEstimations->setChecked(enabled);
  ui->action_outputEqualizer->setChecked(enabled);
  ui->action_outputPhaseOffset->setChecked(enabled);
  ui->action_outputConstellation->setChecked(enabled);
}

void MainWindow::enableMeasurements()
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::qamBuf, ui->action_outputMER->isChecked());
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::encBuf, ui->action_outputBER0->isChecked());
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::dataBuf, ui->action_outputBER->isChecked());
}

void MainWindow::setMeasurementsEnabled(bool enabled)
{
  ui->action_outputMER->setChecked(enabled);
  ui->action_outputBER0->setChecked(enabled);
  ui->action_outputBER->setChecked(enabled);
}

void MainWindow::on_button_connectionControl_clicked()
{
  if(navdatIf.isEnabled() || IsWaitConnection)
    closeConnection();
  else
    openConnection();
}

void MainWindow::on_action_outputEnableAll_triggered()
{
  setSignalsEnabled(true);
  enableSignals();
}

void MainWindow::on_action_outputDisableAll_triggered()
{
  setSignalsEnabled(false);
  enableSignals();
}

void MainWindow::on_action_outputEnableMeasurement_triggered()
{
  if (measurementFileName.isEmpty())
    on_action_measurementSetFile_triggered();
  setMeasurementsEnabled(true);
  enableMeasurements();
}

void MainWindow::on_action_outputDisableMeasurement_triggered()
{
  setMeasurementsEnabled(false);
  enableMeasurements();
}

void MainWindow::setSignalDetectedData(quint32 Idx)
{
  ui->StartIdx->setText(QString::number(Idx));
}

void MainWindow::setEstimationsData(float snr, float freqOffset)
{
  ui->SNR->setText(QString::number(snr,'f',2));
  ui->FreqOffset->setText(QString::number(freqOffset,'f',2));
}

#include "sdr_math.h"
template<typename T>
static int cmp_vectors(QVector<T> *data0, QVector<T> *data, quint32 &size)
{
  int bitErrCount = 0;
  if (data0->size() < data->size())
    size = data0->size();
  else
    size = data->size();
  for (quint32 i = 0; i < size; ++i)
  {
    Value_t x0 = data0->at(i), x = data->at(i);
    if (x0 != x)
      bitErrCount += hamming_distance(x0, x);
  }
  return bitErrCount;
}

void MainWindow::setQamData(quint8 xS, QVector<qint8> data)
{
  qint8 factor;
  QVector<qint8> *data0;
  QLabel* target;
  switch(xS)
  {
  default: return;

  case ReceiverSignalsNavdatInterface::MIS:
    factor = 2;
    data0 = &measurementMER.misData;
    target = ui->misMER;
    break;

  case ReceiverSignalsNavdatInterface::TIS:
    factor = measurement_tisQamBitsCount;
    data0 = &measurementMER.tisData;
    target = ui->tisMER;
    break;

  case ReceiverSignalsNavdatInterface::DS:
    factor = measurement_dsQamBitsCount;
    data0 = &measurementMER.dsData;
    target = ui->dsMER;
    break;
  }
  quint32 size;
  int bitErrCount = cmp_vectors(data0, &data, size);
  target->setText(QString::number((int)((bitErrCount*100.0)/(factor*size) + 0.5))+"%");
}

void MainWindow::setEncData(quint8 xS, QVector<quint8> data)
{
  QVector<quint8> *data0;
  QLabel* target;
  switch(xS)
  {
  default: return;

  case ReceiverSignalsNavdatInterface::MIS:
    data0 = &measurementBER0.misData;
    target = ui->misBER0;
    break;

  case ReceiverSignalsNavdatInterface::TIS:
    data0 = &measurementBER0.tisData;
    target = ui->tisBER0;
    break;

  case ReceiverSignalsNavdatInterface::DS:
    data0 = &measurementBER0.dsData;
    target = ui->dsBER0;
    break;
  }
  quint32 size;
  int bitErrCount = cmp_vectors(data0, &data, size);
  target->setText(QString::number((int)((bitErrCount*100.0)/(8*size) + 0.5))+"%");
}

void MainWindow::setData(quint8 xS, QVector<quint8> data)
{
  QVector<quint8> *data0;
  QLabel* target;
  switch(xS)
  {
  default: return;

  case ReceiverSignalsNavdatInterface::MIS:
    data0 = &measurementBER.misData;
    target = ui->misBER;
    break;

  case ReceiverSignalsNavdatInterface::TIS:
    data0 = &measurementBER.tisData;
    target = ui->tisBER;
    break;

  case ReceiverSignalsNavdatInterface::DS:
    data0 = &measurementBER.dsData;
    target = ui->dsBER;
    break;
  }
  quint32 size;
  int bitErrCount = cmp_vectors(data0, &data, size);
  target->setText(QString::number((int)((bitErrCount*100.0)/(8*size) + 0.5))+"%");
}

void MainWindow::on_action_outputScope_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 2) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Scope, enabled);
}

void MainWindow::on_action_outputSpectrum_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 0) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Spectrum, enabled);
}

void MainWindow::on_action_outputSyncStartIdx_triggered(bool enabled)
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::SignalDetected, enabled);
}

void MainWindow::on_action_outputSyncEstimations_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 1) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::SyncEstimations, enabled);
}

void MainWindow::on_action_outputEqualizer_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 0) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Equalizer, enabled);
}

void MainWindow::on_action_outputPhaseOffset_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 0) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::PhaseOffset, enabled);
}

void MainWindow::on_action_outputEstimations_triggered(bool enabled)
{
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Estimations, enabled);
}

void MainWindow::on_action_outputConstellation_triggered(bool enabled)
{
  if(ui->tabSignals->currentIndex() != 0) return;
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::Constellation, enabled);
}

#include <QDataStream>
template<typename T>
static QVector<T> settings_Array(QSettings &settings, QString key)
{
  QByteArray array = settings.value(key).toByteArray();
  QVector<T> vector(array.size()/sizeof(T));

  QDataStream out(&array, QIODevice::ReadOnly);
  out.setVersion(QDataStream::Qt_5_5);
  for (int i = 0; i <vector.size(); ++i)
  {
    T x;
    out >> x;
    vector[i] = x;
  }
  return vector;
}

void MainWindow::on_action_measurementSetFile_triggered()
{
  measurementFileName = QFileDialog::getOpenFileName(this, "Открыть файл для измерений", "", "*.navdat");
  QSettings settings(measurementFileName, QSettings::IniFormat);
  if (settings.value("frame_count").toInt() != 1)
  {
    message("Измерения", "Поддерживаются только однопакетные файлы");
    return;
  }

  measurement_tisQamBitsCount = settings.value("tis_qam_mode").toInt();
  measurement_dsQamBitsCount = settings.value("ds_qam_mode").toInt();

  measurementMER.misData = settings_Array<qint8>(settings, "frame_0/mis_qam_data");
  measurementMER.tisData = settings_Array<qint8>(settings, "frame_0/tis_qam_data");
  measurementMER.dsData  = settings_Array<qint8>(settings, "frame_0/ds_qam_data");

  measurementBER0.misData = settings_Array<quint8>(settings, "frame_0/mis_enc_data");
  measurementBER0.tisData = settings_Array<quint8>(settings, "frame_0/tis_enc_data");
  measurementBER0.dsData  = settings_Array<quint8>(settings, "frame_0/ds_enc_data");

  measurementBER.misData = settings_Array<quint8>(settings, "frame_0/mis_data");
  measurementBER.tisData = settings_Array<quint8>(settings, "frame_0/tis_data");
  measurementBER.dsData  = settings_Array<quint8>(settings, "frame_0/ds_data");

}

void MainWindow::on_action_outputMER_triggered(bool enabled)
{
  if (measurementFileName.isEmpty())
    on_action_measurementSetFile_triggered();
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::qamBuf, enabled);
  ui->widgetMER->setVisible(enabled);
}

void MainWindow::on_action_outputBER0_triggered(bool enabled)
{
  if (measurementFileName.isEmpty())
    on_action_measurementSetFile_triggered();
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::encBuf, enabled);
  ui->widgetBER0->setVisible(enabled);
}

void MainWindow::on_action_outputBER_triggered(bool enabled)
{
  if (measurementFileName.isEmpty())
    on_action_measurementSetFile_triggered();
  emit setSignalEnabled(ReceiverSignalsNavdatInterface::dataBuf, enabled);
  ui->widgetBER->setVisible(enabled);
}

void MainWindow::on_tabSignals_currentChanged(int index)
{
  Q_UNUSED(index);
  enableSignals();
}

void MainWindow::on_RefreshTime_valueChanged(double arg1)
{
  refreshTimer.setInterval(arg1*1000);
}

void MainWindow::on_ScopeTime_valueChanged(double arg1)
{
  ScopePlot->setRefreshInterval(arg1);
  ScopePlot->setTimeInterval(arg1);
  emit setScopeParams();
}
